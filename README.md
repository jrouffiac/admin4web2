***Admin4Web2 for OS/2***

GUI to administer the Web/2 web server. Written in VisPro/Rexx

Copyright: Jean-Yves Rouffiac 2017-2025
License: GNU GPL v3

---

Both Web/2 and VisPro/Rexx are FREEWARE but neither is supported any longer.

Web/2 1.3b5 (which I believe is the latest version) is 
available from the [hobbes](https://hobbesarchive.com/) OS/2 archive:  
[Download](https://hobbesarchive.com/Home/Download?path=/Hobbes/pub/os2/server/web/Web2_1-3b5.zip)  
[Details](https://hobbesarchive.com/?detail=/pub/os2/server/web/Web2_1-3b5.zip)



Likewise, VisPro/Rexx 3.11 Gold edition is also available from hobbes:  
[Download](https://hobbesarchive.com/Home/Download?path=/Hobbes/pub/os2/dev/proglang/rexx/VisProREXX_3-1-1.wpi)  
[Details](https://hobbesarchive.com/?detail=/pub/os2/dev/proglang/rexx/VisProREXX_3-1-1.wpi)  

**IMPORTANT: The VisPro/REXX serial number will be displayed early on in the installation process, be sure to make a note of it as it is not shown again on the registration page of the installer.**
